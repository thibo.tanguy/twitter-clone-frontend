import './App.css'
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import PostsList from "./components/PostsList.tsx";

function App() {

    return (
        <>
            <div className="App">
                <Router>
                    <Routes>
                        <Route path="/" element={<PostsList/>}/>
                        <Route path="/posts" element={<PostsList/>}/>
                    </Routes>
                </Router>
            </div>
        </>
    )
}

export default App
