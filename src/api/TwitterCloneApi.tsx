import {Post} from "../models/Post.tsx";

const API_PROD_BASE_URL = '/api';
// const API_DEV_BASE_URL = 'http://localhost:5270//api';

export async function findAllPosts(): Promise<Post[]> {
    const response = await fetch(`${API_PROD_BASE_URL}/posts`);
    return await response.json();
}

export async function savePost(post: Post): Promise<void> {
    await fetch(`${API_PROD_BASE_URL}/posts`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(post),
    });
}

export async function updatePost(post: Post): Promise<void> {
    await fetch(`${API_PROD_BASE_URL}/posts/${post.id}`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(post),
    });
}

export async function deletePost(postId: number): Promise<void> {
    await fetch(`${API_PROD_BASE_URL}/posts/${postId}`, {
        method: 'DELETE',
    });
}
