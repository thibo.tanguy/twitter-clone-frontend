import {User} from "./User.tsx";
import {PostLike} from "./PostLike.tsx";
import {Comment} from "./Comment.tsx";
import {PostImage} from "./PostImage.tsx";

export interface Post {
    id: string,
    title: string,
    content: string,
    date: string,
    user: User,
    images: PostImage[],
    likes: PostLike[],
    comments: Comment[]
}