export interface PostImage {
    id: string,
    url: string,
    alt: string
}