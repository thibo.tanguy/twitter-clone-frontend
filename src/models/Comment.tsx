import {User} from "./User.tsx";
import {CommentLike} from "./CommentLike.tsx";

export interface Comment {
    id: string,
    content: string,
    date: string,
    user: User
    likes: CommentLike[]
}