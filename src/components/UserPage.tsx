import {User} from "../models/User.tsx";

interface UserPageProps {
    user: User;
}

function UserPage({user}: UserPageProps) {
    return (
        <div className="post-content">
            <h1>{user.username}</h1>
        </div>
    );
}

export default UserPage;