import {JSX, useEffect, useState} from "react";
import {findAllPosts} from '../api/TwitterCloneApi.tsx';
import {Post} from "../models/Post.tsx";
import PostItem from "./PostItem.tsx";
import '../styles/PostsList.css';

function PostsList(): JSX.Element {
    const [posts, setPosts] = useState<Post[]>([]);

    useEffect(() => {
        findAllPosts().then(posts => setPosts(posts));
    }, []);

    return (
        <ul className="posts-list">
            <h1>TEST</h1>
            { posts.map(post => <PostItem key={post.id} post={post}/>) }
        </ul>
    );
}

export default PostsList;
