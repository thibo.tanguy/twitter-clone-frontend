import {Comment} from "../models/Comment.tsx";
import '../styles/CommentItem.css';

interface CommentItemProps {
    comment: Comment;
}

function CommentItem({comment}: CommentItemProps) {
    return (
        <li className="comment-item">
            <div className="comment-user">
                <img src={comment.user.image} alt={comment.user.username} className="user-image" />
                <h3 className="user-username">{comment.user.username}</h3>
            </div>
            <p className="comment-content">{comment.content}</p>
            <p className="comment-date">{comment.date}</p>
        </li>
    );
}

export default CommentItem;
