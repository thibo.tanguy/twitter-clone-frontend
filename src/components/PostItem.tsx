import {Post} from "../models/Post.tsx";
import CommentList from "./CommentsList.tsx";
import '../styles/PostItem.css';

interface PostItemProps {
    post: Post;
}

function PostItem({ post }: PostItemProps) {
    return (
        <li className="post-item">
            <div className="post-header">
                <img className="user-image" src={post.user.image} alt={post.user.username}/>
                <p className="username">{post.user.username}</p>
            </div>
            <div className="post-content">
                <p>{post.content}</p>
            </div>
            <img className="post-image" src={post.images[0].url} alt={post.images[0].alt}/>
            <h3>Comments</h3>
            <CommentList comments={post.comments} />
        </li>
    );
}

export default PostItem;
