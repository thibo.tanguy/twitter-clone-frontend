import CommentItem from './CommentItem';
import {Comment} from "../models/Comment.tsx";
import '../styles/CommentsList.css';

interface CommentsListProps {
    comments: Comment[];
}

function CommentList({comments}: CommentsListProps) {
    return (
        <ul className="comments-list">
            {comments.map(comment => (
                <CommentItem key={comment.id} comment={comment}/>
            ))}
        </ul>
    );
}

export default CommentList;
